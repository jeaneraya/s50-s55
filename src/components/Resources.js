import { Row, Button, Accordion } from 'react-bootstrap';

export default function Resources(){
	return (
		<Accordion flush>
	    	<Accordion.Item eventKey="0">
	        	<Accordion.Header>Introduction</Accordion.Header>
	        		<Accordion.Body>
			          If you want to display static content, for example, a set of images, then HTML can do the job for you. However, static pages are slowly becoming a thing of the past. Most of the content today is interactive and includes flashy slideshows, forms, and menus. They enhance user experience and add dynamicity to the website.<br/>

        			<br/>
			          <Button className="primary">Read More</Button>
			        </Accordion.Body>
	      		</Accordion.Item>
      <Accordion.Item eventKey="1">
        	<Accordion.Header>Conclusion</Accordion.Header>
        		<Accordion.Body>
		          In this JavaScript Tutorial, we have learnt how to access an HTML DOM element using its id, and change its innerhtml text.<br/>

        			<br/>

		          <Button className="primary">Read More</Button>
		        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="2">
        	<Accordion.Header>Security</Accordion.Header>
        		<Accordion.Body>
        		JavaScript and the DOM provide the potential for malicious authors to deliver scripts to run on a client computer via the Web. Browser authors minimize this risk using two restrictions. First, scripts run in a sandbox in which they can only perform Web-related actions, not general-purpose programming tasks like creating files.<br/>

        			<br/>
		          <Button className="primary">Read More</Button>
		        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
	)
}