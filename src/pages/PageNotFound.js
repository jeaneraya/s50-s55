import { Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner'

export default function PageNotFound() {

	const data = {
		title: "404 Page Not Found",
		content: "The Page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}

	return(

		<Banner data={data}/>

	)
}